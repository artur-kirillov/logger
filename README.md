# README #

* Description: 
A custom async logging module for node.js. It's using **winston** and **winston-mongodb** modules. Logger can separate your records by verbosity levels and names of modules.

### How do I get set up? ###
If you want to include this module for your project you should add `"logger": "git+https://bitbucket.org/artur-kirillov/logger.git"` to **package.json** and type `npm install` in the terminal.

### Using ###
Just add `var logger = require('logger')('module');` (where 'module' custom name that should be displaying) in your file that wants to log something.
Also you should invoke `require('logger').setConfig(config);` where `config` should be json file.
Logger supports next levels:
```
#!javascript
levels: {
  silly: 0,
  verbose: 1,
  info: 2,
  warn: 3,
  debug: 4,
  error: 5
}
```
This describes how can you use all levels.
```
#!javascript
logger.log('silly', message);
logger.log('verbose', message);
logger.log('info', message);
logger.log('warn', message);
logger.log('debug', message);
logger.log('error', message);
```
But I prefer shorthand. Example below.
```
#!javascript
logger.silly(message);
logger.verbose(message);
logger.info(message);
logger.warn(message);
logger.debug(message);
logger.error(message);
```
Also added `logger.report()`. It's just synonym for 'info' level.

```
#!javascript
var data = 'message';
logger.report("Just add the", data, "that you want to log.");
```
All methods can take a list of parameters that will be concat in one string.

All levels colorized. It means that if you use `logger.warn()` color of level and message will be yellow. For `logger.error()` it will be red, etc.
Colors of modules' names are setting from this list.
```
#!javascript
colors = [
    "blue",
    "cyan",
    "green",
    "magenta",
    "red",
    "yellow",
    "white"
  ];
```

####  Dependencies: 
```
#!javascript
"colors": "^0.6.2",
"lodash": "^2.4.1",
"moment": "^2.8.1",
"winston": "^0.7.3",
"winston-mongodb": "^0.5.1"
```

If you don't have these modules in the **'node-modules'**, logger will install them into his own **'node-modules'**.
Also you can config the name of database and collection in config.json.