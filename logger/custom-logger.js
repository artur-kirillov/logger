'use strict';

var
  winston = require('winston'),
  util = require('util');

var CustomLogger  = function (options) {
  winston.Logger.call(this, options);
  this._module = options.module;
};

util.inherits(CustomLogger, winston.Logger);
CustomLogger.prototype.name = 'customLogger';

CustomLogger.prototype.log = function(level){
  var args = Array.prototype.slice.call(arguments).concat({ "moduleName": this._module });
  winston.Logger.prototype.log.apply(this, args);
};

module.exports = CustomLogger;