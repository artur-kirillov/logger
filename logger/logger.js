'use strict';

var
  _ = require('lodash'),
  winston = require('winston'),
  moment = require('moment'),
  CustomLogger = require('./custom-logger'),
  CustomTransport = require('./custom-transport'),
  CustomTransportREST = require('./custom-transport-rest');

var
  loggers = {},
  _config;

function setConfig(config) {
  _config = config;
}

var Logger = function (module) {
  if (!_config) {
    _config = {};
    //throw new Error('Configuration was not set. Use: require("logger").setConfig()')
  }

  module = module || 'default';

  if (_.has(loggers, module)) {
    return loggers[module];
  }

  var logger = new (CustomLogger)({
    transports: [
      new (CustomTransport)({
        colorize:  true,
        silent:    false,
        timestamp: function () {
          return moment().format('YYYY-MM-DD HH:mm:ss.SSS');
        },
        level: _config.console ? _config.console.minimumLevel : 'silly'
      }),
      new (winston.transports.MongoDB)({
        db:         (_config.db && _config.db.db) || 'logger',
        collection: (_config.db && _config.db.collection) || 'logs',
        level:      (_config.db && _config.db.minimumLevel) || 'warn'
      })
    ],
    module: module
  });

  if (!loggers[module]) {
    logger.report = logger.info;
  }

  loggers[module] = logger;

  return logger;
};

var LoggerREST = function () {
  var logger = new (winston.Logger)({
    transports: [
      new (CustomTransportREST)({
        db:         (_config.db && _config.db.db) || 'logger',
        collection: (_config.db && _config.db.collection_rest) || 'requests'
      })
    ]
  });

  logger.logRequest = logger.info.bind(logger.info, '');
  return logger;
};

module.exports = Logger;
module.exports.setConfig = setConfig;
module.exports.createRestLogger = LoggerREST;