'use strict';

var
  util = require('util'),
  common = require('winston/lib/winston/common'),
  Transport = require('winston/lib/winston/transports/transport').Transport;

var
  colors = [
    'blue',
    'cyan',
    'green',
    'magenta',
    'red',
    'yellow',
    'white'
  ];

var CustomTransport  = function (options) {
  Transport.call(this, options);
  options = options || {};

  this.json        = options.json        || false;
  this.colorize    = options.colorize    || false;
  this.prettyPrint = options.prettyPrint || false;
  this.timestamp   = typeof options.timestamp !== 'undefined' ? options.timestamp : false;
  this.label       = options.label       || null;

  if (this.json) {
    this.stringify = options.stringify || function (obj) {
      return JSON.stringify(obj, null, 2);
    };
  }
};

util.inherits(CustomTransport, Transport);
CustomTransport.prototype.name = 'customTransport';

CustomTransport.prototype.log = function (level, msg, meta, options) {
  var self = this,
    output;
  if (level === 'error' || level === 'debug') {
    msg = msg.red;
  } else if (level === 'warn'){
    msg = msg.yellow;
  }
  else {
    msg = msg.white;
  }
  msg = meta.moduleName[CustomTransport.getColor(meta.moduleName)] + " " + msg;
  output = common.log({
    colorize:    this.colorize,
    json:        this.json,
    level:       level,
    message:     msg,

    stringify:   this.stringify,
    timestamp:   this.timestamp,
    prettyPrint: this.prettyPrint,
    raw:         this.raw,
    label:       this.label
  });

  process.stdout.write(output + '\n');
  self.emit('logged');
};

CustomTransport.moduleColors = {};

CustomTransport.getColor = function (module) {
  if(!CustomTransport.moduleColors[module]){
    CustomTransport.moduleColors[module] = colors[0];
    colors.push(colors.shift());
  }

  return CustomTransport.moduleColors[module];
};

module.exports = CustomTransport;