'use strict';

var
  util = require('util'),
  winston = require('winston');

// Creates winston.transports.MongoDB
require('winston-mongodb');

var CustomTransportREST  = function (options) {
  winston.transports.MongoDB.call(this, options);
};

util.inherits(CustomTransportREST, winston.transports.MongoDB);
CustomTransportREST.prototype.name = 'customTransportREST';

CustomTransportREST.prototype.log = function (level, msg, req, callback) {
  var
    self = this,
    filteredRequest = {
      method:   req.method,
      sourceIP: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
      URI:      req.protocol + '://' + req.get('host') + req.originalUrl
    };

  process.nextTick(function () {
    if (self.silent) {
      return callback(null, true);
    }

    self.open(function (err) {
      if (err) {
        return onError(err);
      }

      if (self.timeout) {
        if (self.timeoutId) {
          clearTimeout(self.timeoutId);
        }

        self.timeoutId = setTimeout(function () {
          self.close();
        }, self.timeout);
      }

      function onError(err) {
        self.close();
        self.emit('error', err);
        callback(err, null);
      }

      self._db.collection(self.collection, function (err, col) {
        if (err) {
          return onError(err);
        }

        var entry = {};

        entry.timestamp = new Date();
        entry.source = filteredRequest.sourceIP;
        entry.URI = filteredRequest.URI;
        entry.method = filteredRequest.method;

        if (self.storeHost) {
          entry.hostname = self.hostname;
        }
        if (self.label) {
          entry.label = self.label;
        }

        col.save(entry, { safe: self.safe }, function (err) {
          if (err) {
            return onError(err);
          }

          self.emit('logged');
          callback(null, true);
        });
      });
    });
  });
};

module.exports = CustomTransportREST;